module ring

sig Node {next: one Node}

//	1
fact {all n : Node | n not in n.next}
fact {all n0 : Node, n1 : Node | (n0 != n1) => (n0.next != n1.next)}

run {} for exactly 4 Node

/*	1 and 2 both work
//	2
fact {all n: Node | n not in n.next}
fact {all n1:Node, n2:Node | n1.next not in n2.next && n2.next not in n1.next}

run {} for exactly 4 Node
*/
