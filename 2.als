module transitive

pred Transitive[r: univ -> univ] {
	// Slide 37
	some r &&
	r.r in r
}

pred TransitiveCover[R: univ -> univ, r: univ -> univ] {
	Transitive[R] &&
	r in R
}

pred SmallestTransitiveCover[R: univ -> univ, r: univ -> univ] {
	TransitiveCover[R, r] &&
	all a: univ | all b: univ | !TransitiveCover[R-(a->b) , r]
}

assert show {
	all R: univ -> univ | all r: univ -> univ | SmallestTransitiveCover[R,r] <=> R = ^r
}

check show
