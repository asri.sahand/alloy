module spanning

pred isTree [r: univ -> univ] {
	//	irreflexive
	//	no (iden & r)
	
	// tree (no circuit)
	no (iden & ^r)
}

pred spans[r1, r2: univ -> univ] {
	isTree[r1] && 
	
	//	if there is a path between two nodes in r1(graph)
	// sot ther is a path between those nodes in r2(spanning tree)
	all a: univ | all b: univ | (a->b) in r1 => (a->b) in r2
	
}

pred connectedGraph[r: univ -> univ] {
	//	there is a path between a to b or
	//											b to a
	all a, b: univ | a != b => ((a->b) in ^r) || ((b->a) in ^r)
}

pred show[r1, r2, org: univ -> univ] {
	some r1 and some r2 and
		r1 != org &&
		connectedGraph[org] &&
		spans[r1, org] &&
		spans[r2, org]
}

run show
